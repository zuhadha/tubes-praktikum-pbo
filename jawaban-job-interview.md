jawaban 10 soal job interview mata kuliah praktikum pemrograman berorientai objek

# Nomor 1 - Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. 
Pendekatan matematika yang saya gunakan cukup sederhana, beberapa diantaranya adalah penggunaan operator logika lebih besar dari, lebih kecil dari, dan sama dengan untuk melakukan control flow pada program. Algoritma pemrograman yang digunakan tidak terlalu kompleks, karena struktur data yang digunakan dalam program ini hanya sebatas ArrayList, selebihnya menggunakan control flow seperti if else, dan switch case untuk berbagai kasus yang berbeda, berikut beberapa pendekatan matematika dalm algoritma pemrograman yang saya gunakan:
1. menggunakan operator sederhana matematika seperti `+ - () /` untuk menghitung berapa persentase menjawab soal dengan benar
[Class Learning](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/Learning.java)
2. menggunakan `do-while` yang dikombinasikan dengan `for loop` untuk membentuk algoritma pencarian guna mencocokkan akun
[Class SignInSignUp](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/SignInSignUp.java)
3. menggunakan `switch-case` untuk menentukan database mana yang akan diakses dan digunakan
[Class QuestionDB](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/database/QuestionDB.java)
4. menggunakan `do while` untuk algoritma *sign in*
[Class SignInSignUp](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/SignInSignUp.java)
5. ada beberapa lagi yang tidak perlu disebutkan karena sederhana

---

# Nomor 2 - Mampu menjelaskan algoritma dari solusi yang dibuat 
Tugas utama dari program yang saya buat adalah bagaimana program ini dapat menampilkan soal sebagai media pembelaran bahasa. Adapun cara supaya user dapat melakukan pembelajaran adalah dengan membuat akun terlebih dahulu, kurang lebih beginilah algoritma dari solusi permasalahan utama dalam program duolingo:
1.  membuat class Question yang nantinya memiliki beberapa turunan jenis soal seperti soal terjemah dan melengkapi kalimat sehingga objeknya berupa soal nyata.
2.  membuat class QuestionDB untuk menyimpan objek pertanyaan, disimpan dalam struktur data ArrayList
3.  membuat class User dan UserDB yang kurang lebih bertujuan sama yakni membuat dan menginstansiasi user.
4.  membuat algoritma login yang apabila saat pengecekan username tidak ditemukan, akan diarahkan untuk masuk ke algoritma sign in yang nantinya data akan langsung di oper ke UserDB
5.  membuat mekanisme pembelajaran dengan nama Class Learning, berisi perintah untuk menampilkan soal dan meminta jawaban berupa input user seraca looping sampai soal habis dan berakhir dengan tampilan akumulasi soal terjawab dengan persentase
   - [Package Question](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/tree/main/question)
   - [Package User](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/tree/main/user)
   - [Package Database](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/tree/main/database)
   - [Class SignInSignUp](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/SignInSignUp.java)
   - [Class Learning](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/commit/0c2c86011b58747333eb390f53ea45e13415a7c5)

---

# Nomor 3 - Mampu menjelaskan konsep dasar OOP
OOP merupakan konsep pembangunan sebuah program aplikasi yang menerapkan class dan objek sebagai komponen utamanya, dalam setiap usecase aplikasi yang diperlukan, dibangun beberapa class untuk memenuhi kebutuhan tersebut. Dalam class akan ada method method dan method ini dapat membuat sebuah class lebih beragam baik itu penggunaannya atau aksesibilitasnya. Setidaknya ada 4 pilar OOP yang telah dipelajari yaitu inheritance, abstraction, Polymorphism, dan Encapsulation. Secara singkat, berikut penjelasan dari keempat pilar tersebut:
1. inheritance
merupakan konsep di mana sebuah class dapat menurunkan konten (atribut dan method) yang ada di dalamnya ke class yang meng"" ke class tersebut. Sebagai contoh, class b melakukan *extends* kepada class a, maka atribut yang ada di dalam class a dapat diakses oleh class b. Hal ini dapat disebut a sebagai superclass dan b sebagai subclass. intinya, mudahnya adalah seolah olah method pada class a merupakan method class b itu sendiri karena dapat digunakan, tetapi yang perlu digarisbawahi di sini adalah, tidak serta merta sebuah elemen dalam super class itu dapat diakses begitu saja, kita tetap harus mempertimbangkan access modifier dari setiap method tersebut
2. abstraction
merupakan modifier yang diberikan kepada sebuah class yang akan dijadikan superclass abstract, penerapan konsep abstraction secara otomatis menerapkan konsep inheritance karena class abstract tidak dapat dibuatkan objek. class abstract lumrahnya akan memiliki method abstract pula yang mana bertujuan untuk memberi tahu bahwasannya class yang akan meng*extends* ke class abstract tersebut harus dibuatkan method abstract pada class nya, misal ada class abstract bernama manusia yang di dalamnya terdapat methode absctract bernama presensi, maka ketika kita membuat subclass dengan nama mahasiswa dan dosen, kedua subclass tersebut harus dibuatkan method presensi di dalamnya.
3. Polymorphism
setidaknya terbagi 2 yaitu Polymorphism dinamis dan statis. Polymorphism dinamis dapat diterapkan dengan pembentukan dua buah method yang sama pada class yang sama tetapi dengan tipe data return dan atau parameter yang berbeda, perbedaan ini dapat berupa perbedaan tipe data, dan atau jumlah. Sedangkan Polymorphism statis dapat diterapkan dalam proses penurunan method dari superclass, kita dapat menerapkan perintah yang berbeda dengan nama yang sama pada tiap class anak.
4. Encapsulation
merupakan proses kita dalam mengatur hak akses suatu atribut/variable suatu class ataupun method. dengan konsep Encapsulation, kita dapat mendeklarasikan variable dan method dengan nama yang sama pada class yang berbeda, selama hak aksesnya tidak terpenuhi maka program akan berjalan dengan baik. Encapsulation mempermudah kita dalam mengakses sebuah variable dan menentukan batasan" sehingga variable atau method dalam suatu class tidak bisa digunakan di mana mana/sembarangan.

---

# Nomor 4 - Mampu mendemonstrasikan penggunaan Encapsulation secara tepat 
Yang paling menonjol ada pada class Question yang merupakan class abstract yang kemudian diturunkan kepada dua subclassnya yaitu TransArrange dan FillTheBlank, clas turunannya merepresentasikan jenis soal. Banyak method yang disetting protected hak aksesnya supaya tidak dapat diguanakan di luar packagenya.
   - [Class Question](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/Question.java)
   - [Class TransArrange](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/TransArrange.java)
   - [Class FillTheBlank](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/FillTheBlank.java)

---

# Nomor 5 - Mampu mendemonstrasikan penggunaan Abstraction secara tepat 
Kurang lebihnya sama seperti nomor 4, implementasi abstraction baru ada pada class Question yang di dalamnya memiliki method abstract isCorrect dan showQuestion, hal ini dibuat demikian karena pada tipe soal yang berbeda, pasti diperlukan cara menampilkan soal yang berbeda dan pengecekan jawaban yang berbeda, karena tipe data jawaban pada tiap tipe soal pun berpotensi berbeda  di mana yang satu string, yang lainnya int atau sebagainya.
   - [Class Question](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/Question.java)
   - [Class TransArrange](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/TransArrange.java)
   - [Class FillTheBlank](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/FillTheBlank.java)

---

# Nomor 6 - Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat 
Dikarenakan fokus utama dari aplikasi ini adalah mengerjakan soal, maka lagi dan lagi, pengimplementasian dari konsep 4 pilar OOP lainnnya yakni inheritance dan Polymorphism paling tepat digunakan pada class Question dan class turunannya, banyak method pada superclass yang diturunkan pada subclass karena sangat relevan dan dapat digunakan dengan efektif, seperti method setter dan getter, ada juga method showQuestion yang mana ada perubahan pada setiap tipe soal, selain itu ada juga pemenuhan method abstract dari class Question
   - [Class Question](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/Question.java)
   - [Class TransArrange](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/TransArrange.java)
   - [Class FillTheBlank](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/question/FillTheBlank.java)

---

# Nomor 7 - Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?
// tambah usecase class apa saja yang diperlukan untuk menunjang 
**Proses Bisnis aplikasi DUOLINGO**
1. user dapat membuat akun dan masuk ke aplikasi melalui akun
2. user dapat mengakses pembelajaran sesuai path
3. user dapat berkompetisi dengan user lain dalam hal exp yang di dapat
4. user dapat melakukan kustomisasi profil seperti foto, username, dan lainnya
5. user dapat melihat konsistensi user dari jumlah hari beruntun
6. user dapat mengakses pembelajaran di luar path untuk mendapatkan nyawa
7. user dapat membeli item supaya pembelajaran lebih menarik

---

# Nomor 8 - Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table
### Clas Diagram
![class diagram](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/media%20documentation/class_diagram_duolingo.drawio.png)


### Case Table
| No | Usecase | Status |
| -------- | -------- | -------- |
| 1   | aplikasi memungkinkan user untuk masuk ke akun atau membuat akun   | Terimplementasi   |
| 2   | aplikasi memungkinkan user untuk mengakses pembelajaran sesuai progress  | Terimplementasi   |
| 3   | aplikasi memungkinkan user untuk menambah nyawa dengan menyelesaikan pembelajaran   | Belum Terimplementasi  |
| 4   | aplikasi memungkinkan user untuk mencoba lompat ke unit berikutnya dengan menyelesaikan soal.   | Belum Terimplementasi   |
| 5   | aplikasi memungkinkan user untuk mengerjakan level terakhir pada unit berkali kali untuk mendapatkan trophy   | Belum Terimplementasi   |
| 6   | aplikasi memungkinkan user SUPER untuk mendapatkan privilage berupa bebas dari iklan, nyawa infiniti   | Belum Terimplementasi   |
| 7   | aplikasi memungkinkan user untuk dapat mengakses soal soal yang dijawab salah dalam menu latihan   | Belum Terimplementasi   |
| 8   | aplikasi memungkinkan user untuk mengerjakan pembelajaran cerita   | Belum Terimplementasi   |
| 9   | aplikasi memungkinkan user SUPER untuk melakukan pembelajaran dengan soal mendengar saja   | Belum Terimplementasi   |
| 10   | aplikasi memungkinkan user SUPER untuk melakukan pembelajaran dengan soal berbicara saja   | Belum Terimplementasi   |
| 11   | aplikasi memungkinkan user melihat leaderboard (dalam seminggu, berapa exp yang terkumpul)   | Belum Terimplementasi   |
| 12   | aplikasi memungkinkan user berkompetisi naik ke liga berikutnya dengan akumulasi exp   | Belum Terimplementasi   |
| 13   | aplikasi memungkinkan user membeli item untuk keperluan pembelajaran user   | Belum Terimplementasi  |
| 14   | aplikasi memungkinkan user mengganti foto profil, nama, nama pengguna, email,   | Belum Terimplementasi   |
| 15   | aplikasi memungkinkan user mengatur kursus, menghapus, reset    | Belum Terimplementasi  |
| 16   | aplikasi memungkinkan user mengganti kata sandi akun   | Belum Terimplementasi   |
| 17   | aplikasi memungkinkan user melakukan subscription dengan uji coba gratis 14 hari   | belum Terimplementasi  |
| 18   | aplikasi memungkinkan user mengatur notifikasi (akses email, notif harian)   | Belum Terimplementasi  |
| 19   | aplikasi memungkinkan user mengedit target harian   | Belum Terimplementasi   |
| 20   | aplikasi memungkinkan user melihat statistik akun   | Belum Terimplementasi   |
| 21   | aplikasi memungkinkan user melihat pencapaian selama menggunakan duolingo   | Belum Terimplementasi   |


 



 







---

# Nomor 9 - Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video
[Video Demonstrasi](https://youtu.be/UHZdDRoQV1U)

---

# Nomor 10 - Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
1. membuat animasi loading bertujuan untuk memberikan kesan dinamis pada aplikasi, ada proses pergantian screen tanpa input user sama sekali
![Fake Loading Animation](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/media%20documentation/login_loading.gif)

2. membuat tampilan progress bar di atas berbarengan dengan user mengerjakan soal yang mana setiap user berpindah soal ke soal berikutnya, progressnya akan terisi hingga pembelajaran selesai progress bar penuh, karena program masih berbasis CLI, maka progress bar dibuat mirip mirip dengan progress bar pada cmd linux. 
![Progress Bar Animation](https://gitlab.com/zuhadha/tubes-praktikum-pbo/-/blob/main/media%20documentation/progress_bar.gif)
