package question;

import java.util.Scanner;

public class FillTheBlank extends Question{
    String[] ftbOptions;

    public FillTheBlank(String questionText, int answer, String[] multipleAnswer) {
        super(questionText, answer, 2);
        this.ftbOptions = multipleAnswer;
    }

    public String showQuestion () {
        System.out.println("pilih kata yang tepat untuk melengkapi kalimat di bawah!");
        System.out.println(super.getQuestionText());
        return super.getQuestionText();
    }

    public String[] getOptions() {
        int abcIndex=97;

        for (int i = 0; i < 0+ftbOptions.length; i++) {
            char abcChar= (char) abcIndex;
            System.out.println("[" + abcChar + "] " + this.ftbOptions[i]);
            abcIndex++;
        }
        return ftbOptions;
    }

    @Override
    public boolean isCorrect(int userChar) {
        Scanner typeToContinue = new Scanner(System.in);
        boolean questionStatus=false;
        if (userChar==this.getAnswerKey()) {
            questionStatus=true;
            System.out.println("Benar!");
            //this.setIsCorrect(true);
            System.out.print("\n\ntype anything to continue...");
            String nextQuestion = typeToContinue.nextLine();
        } else {
            System.out.print("Salah!");
            printKeyAnswer();
            this.setIsCorrect(false);
        }
        return questionStatus;
    }

}