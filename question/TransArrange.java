package question;

import java.util.Scanner;

public class TransArrange extends Question{
    String[] taOptions; 
    String wordDict;
    String wordMean; 

    public TransArrange(String questionText, int arrangeIndex, String[] words, String wordDictionary, String wordMeaning) {
        super(questionText, arrangeIndex, 1);
        this.taOptions = words;
        this.wordDict = wordDictionary;
        this.wordMean = wordMeaning;
    }

    public String showQuestion () {
        System.out.println("terjemahkan kalimat di bawah ini!");
        System.out.println(super.getQuestionText());
        return super.getQuestionText();
    }

    public void getOptions() {
        for (int i = 0; i < taOptions.length; i++) {
            if (i==3 || i==6 || i==9) {
                System.out.println("");
            }
            System.out.print("[" + i + "] " + this.taOptions[i] + "\t") ;
        }
        //System.out.println("");
    }
 
    public String getWordDict() {
        return this.wordMean;
    }

    @Override
    public boolean isCorrect(int userInput) {
        Scanner typeToContinue = new Scanner(System.in);
        boolean questionStatus=false;
        if (userInput == this.getAnswerKey()) {
            questionStatus=true;
            System.out.println("Benar");
            //this.setIsCorrect(true);
            System.out.print("\n\ntype anything to continue...");
            String nextQuestion = typeToContinue.nextLine();
        } else {
            System.out.print("Kurang Tepat!");
            printKeyAnswer();
            this.setIsCorrect(false); 
        }
        return questionStatus;
    }
}
