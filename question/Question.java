package question;

import java.util.Scanner;

abstract public class Question {
    private String questionText;
    private int answerKey;
    private boolean isCorrect;
    private int questionType;

    protected Scanner readUserAnswer = new Scanner(System.in);
    
    protected Question(String questionText, int answer, int questionsType) {;
        this.questionText = questionText;
        this.answerKey = answer;
        this.questionType = questionsType;
    }

    abstract protected String showQuestion();

    // untuk oper questionText
    protected String getQuestionText() {
        return questionText;
    }

    // dipake untuk percabagan if casting questions ke childClass
    public int getQuestionType() {
        return questionType;
    }

    // untuk cek sola dijawab benar atau salah
    abstract protected boolean isCorrect(int userAnswer);

    protected void setIsCorrect(Boolean userAnswerIs) {
        this.isCorrect=userAnswerIs;
    }

    // cek jawaban


    // untuk perbandingan benar atau salah di method isCorrect pada class anak question
    protected int getAnswerKey() {
        return answerKey;
    }


    // blm
    protected boolean getIsCorrect() {
        return isCorrect; //sementara
    }

    public static void cls() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    protected void printKeyAnswer() {
        Scanner typeToContinue = new Scanner(System.in);
        
        System.out.println("\n=============================");
        System.out.println("jawaban benar adalah "+ this.getAnswerKey());
        System.out.println("=============================\n");
        System.out.print("\n\ntype anything to continue...");
        String nextQuestion = typeToContinue.nextLine();
    }

    public void showProgress(int questionTh, int uLife) {
        int userLife = uLife;
        String lifeDisplay = "life: ";

        String showHealth =lifeDisplay+String.valueOf(uLife)+"\n";

        String done1 = "[#######              ] " + showHealth;
        String done2 = "[##############       ] " + showHealth;
        String done3 = "[#####################] " + showHealth;

        switch (questionTh) {
            case 0:
                System.out.println(done1);
                break;
            case 1:
                System.out.println(done2);
                break;
            case 2:
                System.out.println(done3);
                break;
            default:
                break;
        }
    }

}

// question translate dari inggris ke indo dan sebaliknyadengan menusun kata per kata


// question fill the blank with option

