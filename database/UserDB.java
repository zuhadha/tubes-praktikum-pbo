package database;

import java.util.ArrayList;
import user.UserProfile;
import user.UserStat;

public class UserDB {
    private ArrayList<UserProfile> usersProf;
    private ArrayList<UserStat> usersStat;

    public UserDB() {
       /*  usersProf = instantiateUser();
        usersStat = instantiateUserStat();*/
        usersProf = new ArrayList<>();
        usersStat = new ArrayList<>();
        instantiateUser();
        instantiateUserStat();
    }
    

    public ArrayList<UserProfile> instantiateUser() {
        ArrayList<UserProfile> usersProf = new ArrayList<>();

        usersProf.add(new UserProfile(1, "zuhadha", "juhed", "zuhadzuhad@gmail.com", "zuhad123", false));
        usersProf.add(new UserProfile(2, "moelya", "liza", "haliza27@gmail.com", "jaja123", true));
        usersProf.add(new UserProfile(3, "Lili lili", "lili", "lyly123@gmail.com", "lili123", false));

        return usersProf;
    }

    public ArrayList<UserStat> instantiateUserStat() {
        ArrayList<UserStat> usersStat = new ArrayList<>();

        usersStat.add(new UserStat(1, 2, 1, 2, 5));
        usersStat.add(new UserStat(2, 1, 2, 2, 4));
        usersStat.add(new UserStat(3, 1, 2, 3, 3));

        return usersStat;
    }

    public void addUserAndStat(UserProfile user, UserStat stat) {
        usersProf.add(user);
        usersStat.add(stat);
    }

    public ArrayList<UserProfile> getUsersProf() {
        return usersProf;
    }

}