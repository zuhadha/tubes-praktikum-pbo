package database;
import java.util.ArrayList;

import question.FillTheBlank;
import question.Question;
import question.TransArrange;

public class QuestionDB{
    
    public ArrayList<Question> getQuestionsForQuiz (String levelLessonTh) {
        ArrayList<Question> questions = new ArrayList<>();
        String callLevelLesson = levelLessonTh;
        switch (callLevelLesson) {
            case "1_0":
            // add questions for chapter 1 progress 1
                questions.add(new TransArrange(
                    "[1_0] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "1_1":
            // add questions for chapter 1 progress 1
                questions.add(new TransArrange(
                    "The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "1_2":
                // add questions for progress 1 chapter 2
                questions.add(new TransArrange(
                    "[1_2] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "1_3":
                // add questions for progress 1 chapter 2
                questions.add(new TransArrange(
                    "[1_3] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "2_1":
                // add questions for progress 2 chapter 1
                questions.add(new TransArrange(
                    "[2_1] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "2_2":
                // add questions for progress 2 chapter 2
                questions.add(new TransArrange(
                    "[2_2] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "2_3":
                // add questions for progress 2 chapter 3
                questions.add(new TransArrange(
                    "[2_3] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "3_1":
                // add questions for progress 3 chapter 1
                questions.add(new TransArrange(
                    "[3_1] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "3_2":
                // add questions for progress 3 chapter 2
                questions.add(new TransArrange(
                    "[3_2] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            case "3_3":
                // add questions for progress 3 chapter 3
                questions.add(new TransArrange(
                    "[3_3] The Apple and Jackfruit are on the table", 
                    2976403, 
                    new String[]{"atas", "Mangga", "Apel", "meja", "di  ", "Durian", "ada ", "Nangka", "Saya", "dan ", "bawah"}, 
                    "Jackfruit", 
                    "Nangka"));
                questions.add(new TransArrange(
                    "I like Banana because its color is yellow", 
                    213680, 
                    new String[]{"Kuning", "suka", "Saya", "Pisang", "Hijau", "makan", "karena", "membeli", "warnanya"}, 
                    "Banana", 
                    "Pisang"));
                questions.add(new FillTheBlank(
                    "I _____ a new Laptop", 
                    3, 
                    new String[]{"has", "had", "buying", "have", "already"}));
                break;
            default:
                break;
        }
        return questions;
    }
}
