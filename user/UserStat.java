package user;

import java.beans.ExceptionListener;

public class UserStat {
    // atribut user's progress, stat
    private int userId;
    private int userLife;
    private int currentUnit, currentLevel, currentLesson;
    private int dayStreak;
    private int totalXp;
    private int dailyXp;

    public UserStat(int uId, int uUnit, int uLevel, int uLesson, int uLife) {
        this.userId = uId;
        this.currentUnit = uUnit;
        this.currentLevel = uLevel;
        this.currentLesson = uLesson;
        this.userLife = uLife;
    }
    

    // setter
    public void setUnit(int passUnit) {
        this.currentUnit = passUnit;
    }
    
    public void setLevel(int passLevel) {
        this.currentLevel = passLevel;
    }
    
    protected void setLesson(int passLesson) {
        this.currentLesson = passLesson;
    }

    // getter
    public int getUnit () {
        return this.currentUnit;
    }
    
    public int getLevel () {
        return this.currentLevel;
    }
    
    public int getLesson () {
        return this.currentLesson;
    }

    public int getUserId() {
        return this.userId;
    }

    // akses nyawa
    public void setLife (int passLife) {
        this.userLife = passLife;
    }

    public int getLife () {
        return  this.userLife;
    }

    //tambah nyawa (kayanya ini bisa di override ganti parameter pake boolean berhasil selesain pelajaran tambah)
    public void addLife () {
        this.userLife++;
    }

    public void updateLife (int lifeAfterLearning) {
        this.userLife=lifeAfterLearning;
    }

    // day streak
    public void setDayStreak(int passDayStreak) {
        this.dayStreak = passDayStreak;
    }

    public int getDayStreak() {
        return this.dayStreak;
    }

    public int getDailyXp() {
        return dailyXp;
    }

    public void addTotalXp(int passDailyXp) {
        this.totalXp+= passDailyXp;
    }

    public int getTotalXp() {
        return this.totalXp;
    }

    public void updateStat(int life, int exp) {
        this.userLife = life;
        this.dailyXp+=exp;
    }


    public Object getScore() {
        return null;
    }
}
