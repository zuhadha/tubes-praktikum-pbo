package user;

import java.util.Scanner;

public class UserProfile {
    // atribut user's profile
    private int userId;
    private String userName;
    private String nickName;
    private String userEmail;
    private String userPassword;
    private boolean isNewUser = false;
    private boolean userSubscription;



    public UserProfile(int uId, String uName, String uNick, String uEmail, String uPass, Boolean uSubs){
        this.setProfile(uId, uName, uNick, uEmail, uPass, uSubs);
    }

    public UserProfile(int uId, String uName, String uNick, String uEmail, String uPass, Boolean uSubs, boolean isNewUser){
        this.setProfile(uId, uName, uNick, uEmail, uPass, uSubs);
        this.isNewUser = true;
    }

    private void setProfile(int uId, String uName, String uNick, String uEmail, String uPass, boolean uSubs) {
        this.userName = uName;
        this.nickName = uNick;
        this.userEmail = uEmail;
        this.userPassword = uPass;
        this.userSubscription = uSubs;
    }

    public void setUserName(String unameInput) {
        this.userName = unameInput;
    }

    public void setNickname(String nicknameInput) {
        this.nickName = nicknameInput;
    }

    public void setUserEmail(String emailInput) {
        this.userEmail = emailInput;
    }

    public void setUserPassword(String passInput) {
        this.userPassword = passInput;
    }

    public int getUserId() {
        return this.userId;
    }

    public int getUserId(int i) {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getNickName() {
        return this.nickName;
    }

    public String getPassword() {
        return this.userPassword;
    }
    public boolean getIsNewUser() {
        return this.isNewUser;
    }

    public boolean isUnameEqual(String unameInput) {
        boolean isTrue = false;
        if (this.userName.equals(unameInput)) {
            isTrue=true;
        }
        return isTrue;
    }

    public boolean isPassEqual(String unameInput, String passInput) {
        boolean isTrue = false;
        if (this.userName.equals(unameInput) && this.userPassword.equals(passInput)) {
            isTrue = true;
        }
        return isTrue;
    }

    public boolean isEmailEqual(String emailInput) {
        boolean isTrue = false;
        if (this.userEmail.equals(emailInput)) {
                isTrue = true;
        }
        return isTrue;
    }
    
    public Scanner readPassword = new Scanner(System.in);
    protected void changePassword() {
        String tempPassword;
        boolean changePassStatus = false;


        System.out.print("new password\t\t:");
        this.userPassword = readPassword.nextLine();

        do {
            System.out.print("confirm new password\t:");
            tempPassword = readPassword.nextLine();
            if (this.userPassword.equals(tempPassword)) {
                changePassStatus = true;
                System.out.println("Password changed successfully!");
                break;
            } else {
                char userChoice='a';
                System.out.print("try the confirmation password? (y/n)");
                userChoice = readPassword.nextLine().charAt(0);
                
                switch (userChoice) {
                    case 'y':
                        //System.out.print("confirm new password\t:");
                        //tempPassword = readPassword.nextLine();
                        break;
                    case 'n':
                        System.out.print("new password\t\t:");
                        this.userPassword = readPassword.nextLine();
                        break;
                    default:
                        break;
                }
                break;
            }

        } while (changePassStatus==false);
    }



}
