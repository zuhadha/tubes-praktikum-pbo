import java.util.ArrayList;
import java.util.Scanner;

import database.UserDB;
import user.UserProfile;
import user.UserStat;

public class SignInSignUp {
    UserDB userDatabase = new UserDB();
    ArrayList<UserProfile> users = userDatabase.instantiateUser();
    ArrayList<UserStat> usersDetail = userDatabase.instantiateUserStat();
    
    //String whichUser=null;
    int userLife=0;
    int userLoggedInId=0;
    Scanner readLogin = new Scanner(System.in);
    

    public int signIn() {
        String usernameLogin=null;
        String passwordLogin=null;
        int loginStatus=0; // 1=username tidak ada dalam database; 2=username ada, password salah, 3 username ada password benar

/*         userDatabase.addUserAndStat(
            new UserProfile(5 ,"zuhad","zuhad", "zuhad","zuhad", false, true), 
            new UserStat(5, 1, 1, 0, 5));
*/
        do {
            if (loginStatus==0 || loginStatus==1) {
                if (loginStatus==1) {
                    String register;
                    register = readLogin.nextLine();

                    if (register.equalsIgnoreCase("Y")) {
                        signUp();
                    }
                }
                cls();
                System.out.println("\n==========| SIGN IN |==========");
                System.out.print("username: ");
                usernameLogin = readLogin.nextLine();
                System.out.print("password: ");
                loginStatus=0;
                passwordLogin = readLogin.nextLine();
            } else if (loginStatus==2) {
                System.out.print("password: ");
                passwordLogin = readLogin.nextLine();
            }

            getOutFromFori:
            for (int i = 0; i < users.size(); i++) {
                UserProfile retrieverUser = (UserProfile) users.get(i);
                UserStat retrieveStat = (UserStat) usersDetail.get(i);
                if (retrieverUser.isPassEqual(usernameLogin, passwordLogin)) {
                    loginStatus=3;
                    userLoggedInId = (i+1);
                    System.out.println("userLogInid: " + retrieverUser.getNickName());
                    userLife=retrieveStat.getLife();
                } else if (retrieverUser.isUnameEqual(usernameLogin)) {
                    loginStatus = 2;
                } else  {
                    if (i==(users.size()-1)) {
                    loginStatus = 1;
                    }
                } 
    
    
                switch (loginStatus) {
                    case 3:
                        cls();
                        System.out.println("logging in...");
                        pauseFor(3000);
                        cls();
                        if (retrieverUser.getIsNewUser()) {
                            System.out.println("Selamat datang di Duolingo, " + retrieverUser.getNickName() + "^^");
                        } else {
                        System.out.println("Selamat datang kembali, " + retrieverUser.getNickName() + "!");
                        }
                        break getOutFromFori;

                    case 2:
                        if (i==(users.size()-1)) {
                            System.out.println("Password salah!");
                        }
                        break;

                    case 1: 
                        System.out.print("Username tidak ditemukan, buat akun (Y/N)? ");
                        break;
                
                    default:
                        break;
                }
            
            } 
        } while (loginStatus!=3);
        return getUserLoggedInId();
    }

    private void signUp() {
        String daftarEmail = null;
        do {;
            cls();
            System.out.println("==========| SIGN UP |==========");
            System.out.print("email aktif: ");
            daftarEmail = readLogin.nextLine();
        } while (checkEmail(daftarEmail));

        System.out.print("username: ");
        String daftarUsername = readLogin.nextLine();
        System.out.print("nickname: ");
        String daftarNickname = readLogin.nextLine();
        System.out.print("password: ");
        String daftarPassword = readLogin.nextLine();
        int idNewUser = (users.size()+1);
        users.add(new UserProfile(idNewUser , daftarUsername, daftarNickname, daftarEmail, daftarPassword, false, true));
        usersDetail.add(new UserStat(idNewUser, 1, 1, 0, 5));

        userDatabase.addUserAndStat(
            new UserProfile(idNewUser , daftarUsername, daftarNickname, daftarEmail, daftarPassword, false, true), 
            new UserStat(idNewUser, 1, 1, 0, 5));

        System.out.println("selamat! akun anda berhasil dibuat");
        pauseFor(3000);
    }

    private boolean checkEmail(String candidateEmail) {
        boolean alreadyInUse=false;
        boolean printInvalid = false;
        boolean inUseAndInvalid = false;
        for (int i = 0; i < users.size(); i++) {
            UserProfile retrieverUser = (UserProfile) users.get(i);
            if (retrieverUser.isEmailEqual(candidateEmail)) {
                alreadyInUse=true;
                System.out.println("Email is already in use");
                pauseFor(2000);
            }

            if (!(candidateEmail.contains("@") && (candidateEmail.contains("gmail.com") || candidateEmail.contains(".ac.id"))) && printInvalid==false) {
                System.out.println("Invalid email");
                pauseFor(2000);
                printInvalid=true;
            }
        }
        inUseAndInvalid = alreadyInUse||printInvalid;
        return inUseAndInvalid;
    }

    public int getUserLife(){
        return userLife;
    }

    public int getUserLoggedInId() {
        return userLoggedInId;
    }

    public ArrayList<UserStat> getUserDatabase() {
        return usersDetail;
    }

    private void cls() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private void pauseFor(int miliSecond) {
        try {
            Thread.sleep(miliSecond); // Pause for 2 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
