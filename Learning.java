import java.util.ArrayList;
import java.util.Scanner;


import database.QuestionDB;
import database.UserDB;
import question.FillTheBlank;
import question.Question;
import question.TransArrange;
import user.UserStat;

public class Learning {
    String levelType;
    String levelLesson;
    int userLife;
    int totalProgress;
    int expGained;
    int wrongAnswer;
    int userLearningId;
    Scanner answerUser = new Scanner(System.in);
    

    public Learning(String uLevelLesson, int uLife, int uLearnId) {
        this.levelLesson = uLevelLesson;
        this.userLife = uLife;
        this.userLearningId = uLearnId;
    }

    public void startLesson(ArrayList<UserStat> passUsers) {
        QuestionDB questionDatabase = new QuestionDB();
        ArrayList<Question> questions = questionDatabase.getQuestionsForQuiz(levelLesson);

        cls();
        System.out.println("entering lesson, please wait...");
        pauseFor(4000);
        cls();
        for (int i = 0; i < questions.size(); i++) {
            int intQuestionType = questions.get(i).getQuestionType();
            System.out.println("");
            // print pertanyaan dan pilihan ganda sesuai bentuk classnya.
            showQuestion(intQuestionType, i);
            userAnswer(intQuestionType, i);
        }

        result(questions.size());
        updateStatUserLearning(passUsers);

    }

    public void showQuestion (int questionIndexType, int passI) {
        QuestionDB questionDatabase = new QuestionDB();
        ArrayList<Question> questions = questionDatabase.getQuestionsForQuiz(levelLesson);
        Question retrieveQuestion = (Question) questions.get(passI);

        cls();

        retrieveQuestion.showProgress(passI, userLife);
        if (questionIndexType == 1) {
            final TransArrange retrieverTA = (TransArrange) questions.get(passI);
            retrieverTA.showQuestion();
            retrieverTA.getOptions();
            
        } else if (questionIndexType == 2) {
            final FillTheBlank retrieverFtb = (FillTheBlank) questions.get(passI);
            retrieverFtb.showQuestion();
            retrieverFtb.getOptions();
        }
    }

    public void userAnswer(int questionIndexType, int passI) {
    final Scanner readUserAnswer = new Scanner(System.in);
        QuestionDB questionDatabase = new QuestionDB();
        ArrayList<Question> questions = questionDatabase.getQuestionsForQuiz(levelLesson);

        if (questionIndexType == 1) {
            final TransArrange retrieverTA = (TransArrange) questions.get(passI);
            System.out.print("\nurutan indeks kata: ");
            int passAnswer=0;
            
            //passAnswer = readUserAnswer.nextInt();
            if (readUserAnswer.hasNextInt()) {
                //readUserAnswer.nextLine();
                passAnswer = readUserAnswer.nextInt();  
                System.out.println("you entered: " + passAnswer);
            } else {  
                System.out.println("kok ini keprint");
            }
        
            if (retrieverTA.isCorrect(passAnswer)==false) {
                userLife--;
                wrongAnswer++;
            } else {
                expGained+=20;
            }
            
        } else if (questionIndexType == 2) {
            final FillTheBlank retrieverFtb = (FillTheBlank) questions.get(passI);
            System.out.print("\npilih antara a-e: ");
            char passAnswer = readUserAnswer.next().charAt(0);
            
            if (retrieverFtb.isCorrect(((int) passAnswer-97))==false) {
                userLife--;
                wrongAnswer++;
            }else {
                expGained+=10;
            }
        }
    }
    
    private ArrayList<Integer> result(int totalQuestions) {
        ArrayList<Integer> lifeExp = new ArrayList<>();
        lifeExp.add(userLife);
        lifeExp.add(expGained);
        Question.cls();

        float score = (((totalQuestions-wrongAnswer)*100)/totalQuestions);

        System.out.println("exp: " + expGained);
        System.out.println("score: " + score + "%");

        return lifeExp;
    }

    private void updateStatUserLearning(ArrayList<UserStat> passUsers) {
        ArrayList<UserStat> users = passUsers;
        UserStat userLearning = (UserStat) users.get(userLearningId);
        userLearning.updateStat(userLife, expGained);
    } 

    private void cls () {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private void pauseFor(int miliSecond) {
        try {
            Thread.sleep(miliSecond); // Pause for 2 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}