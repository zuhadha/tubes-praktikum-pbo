import java.util.ArrayList;
import java.util.Scanner;

import user.UserStat;

public class HomePage {

    public static void listLesson (int uLoggedInId, ArrayList<UserStat> passUsers) {
        
        int userLoggedInId=uLoggedInId-1;
        
        ArrayList<UserStat> users = passUsers;
        UserStat userLearning = (UserStat) users.get(userLoggedInId);

        int totalUnit = 3;  // total unit yang ditampilkan
        int totalLevel = 3; // total level dalam 1 unit
        int totalLesson = 3; // jumlah pelajaran dalam 1 level
        int completedLesson = 0;
        int currentUnit = userLearning.getUnit();
        int currentLevel = userLearning.getLevel();
        int currentLesson = userLearning.getLesson(); // sedang berada di pelajaran mana level mana
        

        System.out.println(userLearning.getUnit());

        for (int u = 1; u <= totalUnit; u++) {
            System.out.println("\n\n======| UNIT " + u + " |======\n");
            for (int l = 1; l <= totalLevel; l++) { 
                
                if (u<currentUnit) {
                    completedLesson=totalLesson;
                } else if (u==currentUnit ) {
                    if (l==currentLevel) {
                        completedLesson=currentLesson;
                    } else if (l<currentLevel) {
                        completedLesson=3;
                    } else {
                        completedLesson=0;
                    } 
                } else if (u>currentUnit) {
                    completedLesson=0;
                }
                
                if (l==2) {
                    System.out.println("   Latihan Unit " + u);
                } 
                System.out.println("   Level " + l + " (" + completedLesson + "/" + totalLesson + ")");
            }            
        }
    }

    public static String chooseLevel(int userLoggedInId, ArrayList<UserStat> passUsers) {
        Scanner readLevelUnit = new Scanner(System.in);
        String info = "\n\nchoose the unit and level you want to learn.";

    

        int unitInput;
        int levelInput;

        boolean chooseStatus = false;

        ArrayList<UserStat> users = passUsers;
        UserStat userLearning = (UserStat) users.get(userLoggedInId-1);

        int userUnit = userLearning.getUnit();
        int userLevel = userLearning.getLevel();

        System.out.println(info);
        String message="";
        do {
            System.out.println(message);
            System.out.print("unit: ");
            unitInput = readLevelUnit.nextInt();
            System.out.print("level: ");
            levelInput = readLevelUnit.nextInt();

            if (unitInput>userUnit) {
                message="sorry, you have to complete the previous unit first";
            } else if (unitInput==userUnit) {
                if (levelInput==userLevel) {
                    chooseStatus = true;
                } else if (levelInput>userLevel) {
                    message="sorry, you havent finish the previous level";
                } else {
                    chooseStatus = true;
                }
            } else {
                chooseStatus = true;
            }

            if (unitInput>3) {
                message="sorry, but for now the unit available is only to unit 3";
            } else if (levelInput>3) {
                message="sorry, but for now the level available is only to level 3";
            }
        } while (chooseStatus==false || unitInput>3 || levelInput>3);
        //readLevelUnit.close();
        String unitLevelUser = String.valueOf(unitInput) + "_" + String.valueOf(levelInput);



        return unitLevelUser;
    }


}
