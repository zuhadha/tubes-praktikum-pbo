jawaban Ujian Akhir Semester Praktikum Pemrograman Berorientasi Objek - B

# 1) Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
### Use case user
1. user dapat mengerjakan lessons sesuai batas yang ia kerjakan ketika memiliki nyawa
2. user dapat mengerjakan misi kolaborasi dengan orang lain
3. user dapat menambah nyawa dengan mengerjakan latihan
4. user dapat mencoba melompat ke unit tertentu dengan mengerjakan soal
5. user dapat membeli permata menggunakan uang asli
6. user dapat membeli item virtual menggunakan permata
7. user dapat membuka buku panduan di setiap unit yang berisi frasa pada masing masing unit
8. user dapat melihat kesalahan jawaban secara harian
9. user dapat mengulas materi dengan metode story telling
10. user super dapat melakukan ulasan mendengar
11. user super dapat melakukan ulasan berbicara
12. user dapat melihat ranking liga 
13. user dapat mengganti bahasa yang dipelajari
14. user dapat menambahkan teman
15. user dapat melihat laporan statistik 
16. user dapat melihat pencapaian user
17. user dapata melihat misi teman
18. user dapat menyelesaikan misi teman dan mengklaim hadiah
19. user dapat melihat misi harian
20. user dapat mengklaim misi harian yang telah diselesaikan
21. user dapat melihat feed duolingo

### case manajemen perusahaan
- Manajemen dapat menambahkan bank soal
- manajemen dapat menambahkan list misi harian
- manajemen dapat menentukan harga item 
- manajemen dapat mengupdate etalase toko
- manajemen dapat mengubah tipe akun seseorang (biasa atau super)
- manajemen dapat menambahkan tipe soal

### Use case direksi perusahaan (dashboard, monitoring, analisis)
- direksi perusahaan dapat melihat total pengguna yang terdaftar
- direksi perusahaan dapat melakukan analisis terkait tipe soal apa yang paling efektif dan banyak digunakan
- direksi perusahaan dapat menganalisis sejauh mana orang belajar bahasa melalui aplikasi tersebut

# 2) Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital


# 3) Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle


# 4) Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih


# 5) Mampu menunjukkan dan menjelaskan konektivitas ke database


# 6) Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 


# 7) Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital


# 8) Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital


# 9) Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

